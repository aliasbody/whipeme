# NOTICE:
#
# Application name defined in TARGET has a corresponding QML filename.
# If name defined in TARGET is changed, the following needs to be done
# to match new name:
#   - corresponding QML filename must be changed
#   - desktop icon filename must be changed
#   - desktop filename must be changed
#   - icon definition filename in desktop file must be changed
#   - translation filenames have to be changed

# The name of your application
TARGET = harbour-whipme

CONFIG += sailfishapp

SOURCES += src/harbour-whipme.cpp

OTHER_FILES +=  qml/harbour-whipme.qml \
                qml/cover/CoverPage.qml \
                rpm/harbour-whipme.changes.in \
                rpm/harbour-whipme.spec \
                rpm/harbour-whipme.yaml \
                harbour-whipme.desktop

# to disable building translations every time, comment out the
# following CONFIG line
# CONFIG += sailfishapp_i18n
# TRANSLATIONS += translations/harbour-whipme-de.ts

RESOURCES += \
    Resources.qrc

