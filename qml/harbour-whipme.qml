/*
    WhipMe - A simple Whip reproducer program for the Jolla Phone.

    Copyright (C) 2015 LibreTrend
    Contact: Luis Da Costa <luisdc@libretrend.com>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0
import QtSensors 5.0

ApplicationWindow
{
    cover: Qt.resolvedUrl("cover/CoverPage.qml")

    SensorGesture {
        enabled     : true;

        onDetected: {
            whipSound.play()
        }
    }

    SoundEffect {
        id      : whipSound
        source  : "qrc:/data/resources/sounds/WhipSound.wav"
    }

    MouseArea {
        width   : parent.width
        height  : parent.height

        Image {
            id                      : whipImg;
            source                  : "qrc:/data/resources/images/whipImg.png"
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.verticalCenter  : parent.verticalCenter
        }

        onClicked : {
            whipSound.play()
        }
    }
}
